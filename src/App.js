import React, { Component } from 'react';
import Home from './components/Home';
import Showcase from './components/Showcase'
import Showcase2 from './components/Showcase2'
import Showcase3 from './components/Showcase3'
import Logo from './components/Logo'
import About from './components/About';
import Contact from './components/Contact';
import Nav2 from './components/Nav2';
import Post from './components/Post';
import Navbar from './components/Navbar'
import Footer from './components/Footer'
import Test1 from './components/test1'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import './components/css/Site.css'
import './components/css/Logo.css'
import './components/css/Navbar.css'
import './components/css/Nav2.css'
import './components/css/Showcase.css'
import './components/css/Showcase2.css'
import './components/css/Showcase3.css'
import './components/css/Footer.css'


class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
            <div className="container">
              <Logo/>
              <Navbar/>
              <Showcase/>
              <Nav2/>
              <Showcase2/>
              <Showcase3/>
              <Footer/>
            </div>
            <Switch>
              <Route exact path="/" component={Home}/>
              <Route path="/about" component={About}/>
              <Route path="/contact" component={Contact}/>
              <Route path="/:service_id" component={Post}/>
              <Route path="/test1" component={Test1}/> 
            </Switch>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
