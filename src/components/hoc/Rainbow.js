import React from 'react'

const Rainbow = (WrappedComponent)=>{
    const colours=['red','green','gray','blue','orange'];
    const className=colours[Math.floor(Math.random()*5)]+'-text';
    console.log('classname',className)
    console.log(WrappedComponent,'wrappedComponent')
    return (props)=>{
        return (
            <div className={className}>
            <WrappedComponent {...props}/>
        </div>
        )
    }
}
// https://jsonplaceholder.typicode.com/services

export default Rainbow;