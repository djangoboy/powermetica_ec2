import React, { Component } from 'react'

class Showcase3 extends Component {
  render() {
    return (
      <div className="showcase3">
          <h2>What do we have to offer!</h2>
             <div className="showcase3-content">
                <ul>
                <li>Analytics</li>
                <li>IT Infrastructure</li>
                <li>Services</li>
            </ul>
            <ul>
                <li>Cloud</li>
                <li>Security</li>
            </ul>
            <ul>
                <li>Internet Of Things</li>
                <li>Education</li>
            </ul>
            </div>

             </div>
    )
  }
}

export default Showcase3
