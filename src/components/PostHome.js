import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import Pokeboll from '../pokeball.png'
import './css/Home.css'
import {connect} from 'react-redux'
class PostHome extends Component{
    render(){
        console.log(this.props,'this is hot')
        const services=this.props.services;
        const serviceList=services.length ? (services.map(service=>{
             return (
                    <div className="service card" key={service.id}>
                        <img src={Pokeboll} alt=" this is pokeball"/>
                        <div className="card-content">
                            <Link to={"/"+service.id}><span className="card-title red-text">{service.title}</span></Link>
                            <p>{service.body}</p>
                        </div>
                    </div>  
             )
        })):(<div className="center">No services yet</div>)

        
        return(
            <div className="container PostHome">
                <h4 className="center">PostHome</h4>
                {serviceList}
            </div>
        )
        }
}
const mapStateToProps = (state,addedProps)=>{
    console.log('addedProps',addedProps)
    return {
        services:state.services
    }
}
export default connect(mapStateToProps)(PostHome)