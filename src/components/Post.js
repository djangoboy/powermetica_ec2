import React,{Component} from 'react';
import {connect} from 'react-redux';

class Post extends Component{
    handleClick=()=>{
        this.props.deletePost(this.props.service.id)
        this.props.history.push('/')
    }

    render(){
        console.log('last prob',this.props)
        const service = this.props.service ? (
            <div className="service">
                <div className="center">
                    <h4>{this.props.service.title}</h4>
                    <p>{this.props.service.body}</p>
                    <div className="center">
                        <button className="btn grey" onClick={this.handleClick}>Delete</button>
                    </div>
                </div>
            </div>
        ):(
            <div className="center">Loading service...</div>
        )
        console.log(this.props,'here are props in service')
        return(
            <div className="container">
                <div className="center">{service}</div>
            </div>
        )
    }

}


const mapStateToProps=(state,ownprops)=>{
    let id=ownprops.match.params.service_id;
    console.log(ownprops,id,state.services,'hereererer',)
    return {
        service:state.services.find(service => service.id === id)
    }
}

const mapDispatchToProps=(dispatch)=>{
    return {
        deletePost:(id)=>{dispatch({type:"DELETE_POST",id:id})}
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Post);