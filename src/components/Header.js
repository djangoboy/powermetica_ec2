import React, { Component } from 'react'
import './css/Header.css'

class Header extends Component {
  render() {
    return (
      <header class="header"><p>Quotes of life...!</p><p>Contact</p><p>Email</p></header>
    )
  }
}

export default Header
