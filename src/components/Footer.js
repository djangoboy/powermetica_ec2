import React, { Component } from 'react'

class Footer extends Component {
  render() {
    return (
      <div className="footer">
        <h2>Footer Heading</h2>
        <div id="footer-content">
            <ul>
                <li>Browse Our Services</li>
                <li>Single Page Application Development</li>
                <li>CRM Development</li>
            </ul>
            <ul>
                <li>Browse Our Services</li>
                <li>Single Page Application Development</li>
                <li>CRM Development</li>
            </ul>
            <ul>
                <li>Lorem ipsum dolor sit amet.</li>
                <li>Lorem ipsum dolor sit amet.</li>
                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam?</li>
            </ul>
        </div>
        
      </div>
    )
  }
}

export default Footer
