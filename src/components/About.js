import React from 'react'

const About = ()=>{
    return(
        <div className="container">
            <h4 className="center">About</h4>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Temporibus inventore commodi, sed quidem quod nulla earum sequi ab mollitia. Vel eligendi tenetur excepturi perferendis? Aspernatur quisquam non quaerat in aut.</p>
        </div>
    )
}

export default About