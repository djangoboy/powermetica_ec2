import React, { Component } from 'react'

class Nav2 extends Component {
  render() {
    return (
      <div className="nav2">
          <div>Build A Custom Solution</div>
          <div>Industry Specific</div>
          <div>Hire A Team</div>
          <div>Explore Our Services</div>
      </div>
    )
  }
}

export default Nav2
